package com.company;

import com.company.data.Mappings;
import com.company.data.ProductCodes;
import com.company.data.ProductPropertyKeys;

import java.util.List;
import java.util.Map;

/**
 * Explanation: I created a class for the data structure of the mapping. There is the data stored as a map of maps. Both are EnumMaps
 * For the Keys of the maps, enums are used, so they are all defined.
 * As the specification not defines if the expected output should be ordered(I interpreted the expected output as a default println),
 * I also just printed the list.
 * If a specific order is necessary, a method could just get the values of the data structure in that order.
 * A difficulty was the identification of products in the expected output, as there is no product code.
 * I chose to use my data structure class as intermediate result ,so the product code is still there to identify the product.
 * In that intermediate result, a quantity property was added.
 * As it is a map, the access is still efficient.
 * At the end, the map values where streamed and collected in a list, which is returned.
 */
public class Main {

    /**
     * Main method, sets up the environment and performs the given task.
     */
    public static void main(String[] args) {
        // add the list of purchased product codes
        List<ProductCodes> productCodesList = List.of(ProductCodes.CVCD, ProductCodes.SDFD, ProductCodes.DDDF, ProductCodes.SDFD);

        // create mapping, and add values
        Mappings mappings = new Mappings();
        mappings.addEntry(ProductCodes.CVCD, ProductPropertyKeys.version, "1");
        mappings.addEntry(ProductCodes.CVCD, ProductPropertyKeys.edition, "X");
        mappings.addEntry(ProductCodes.SDFD, ProductPropertyKeys.version, "2");
        mappings.addEntry(ProductCodes.SDFD, ProductPropertyKeys.edition, "z");
        mappings.addEntry(ProductCodes.DDDF, ProductPropertyKeys.version, "1");

        // method
        aggregateList(productCodesList, mappings);
        System.out.println(aggregateList(productCodesList, mappings));
    }

    /**
     * Method to perform the given task.
     * Creates a new instance of the Mappings data structure.
     * Iterates over list of purchased product codes.
     * Copy entries to result and add quantity property.
     * Return list of maps, without product code.
     *
     * @return Aggregated list of purchased products and quantity
     */
    private static List<Map<ProductPropertyKeys, String>> aggregateList(List<ProductCodes> productCodes, Mappings mappings) {
        // create result mapping
        Mappings result = new Mappings();

        // iterate over list of purchased product codes
        for (ProductCodes pCode : productCodes) {
            // if product code already exists, increase quantity
            if (result.containsProductCode(pCode)) {
                int quantity = Integer.parseInt(result.getEntry(pCode).get(ProductPropertyKeys.quantity)) + 1;
                result.getEntry(pCode).put(ProductPropertyKeys.quantity, String.valueOf(quantity));
                // if product code doesn't exist yet, add to result
            } else {
                mappings.copyEntriesTo(pCode, result);
                result.addEntry(pCode, ProductPropertyKeys.quantity, "1");
            }
        }

        return result.getEntries();
    }

}

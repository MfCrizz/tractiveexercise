package com.company.data;

/**
 * Enum, to store all possible product property keys
 */
public enum ProductPropertyKeys {
    version, edition, quantity
}

package com.company.data;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Data structure to store a map of mappings.
 * Maps a product code to another map, which maps Product properties to string values.
 */
public class Mappings {
    private final Map<ProductCodes, Map<ProductPropertyKeys, String>> outerMap;

    /**
     * Creates a new Mapping and initializes the map.
     */
    public Mappings() {
        this.outerMap = new EnumMap<>(ProductCodes.class);
    }

    /**
     * Adds an entry to the data structure.
     */
    public void addEntry(ProductCodes productCode, ProductPropertyKeys key, String value) {
        // if an entry for a given product code already exist, add key and value
        if (outerMap.containsKey(productCode)) {
            this.outerMap.get(productCode).put(key, value);
            // if not, create a new inner map and add key-value pair.
        } else {
            final Map<ProductPropertyKeys, String> map = new EnumMap<>(ProductPropertyKeys.class);
            map.put(key, value);
            this.outerMap.put(productCode, map);
        }
    }

    /**
     * Get the Map from a Product code.
     *
     * @return value from map of product codes
     */
    public Map<ProductPropertyKeys, String> getEntry(ProductCodes key) {
        return outerMap.get(key);
    }

    /**
     * Returns true, if the data structure contains a product code.
     */
    public boolean containsProductCode(ProductCodes key) {
        return outerMap.containsKey(key);
    }

    /**
     * Method, to copy an entry of the map to another Mapping data structure.
     */
    public void copyEntriesTo(ProductCodes key, Mappings otherMappings) {
        if (outerMap.containsKey(key)) {
            outerMap.get(key).forEach((key1, value) -> otherMappings.addEntry(key, key1, value));
        }
    }

    /**
     * Method to get the required structure.
     *
     * @return List of maps. e.g.: Without the product code
     */
    public List<Map<ProductPropertyKeys, String>> getEntries() {
        return outerMap.values().stream().toList();
    }

}

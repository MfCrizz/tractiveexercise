package com.company.data;

/**
 * Enum, to store all possible product codes
 */

public enum ProductCodes {
    CVCD, SDFD, DDDF
}
